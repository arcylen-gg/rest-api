<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Routes for API */
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Auth::routes();
// Route::resource('posts', 'PostController');
// Route::resource('posts.comments', 'CommentController');
// Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'posts' , 'middleware' => 'auth:api'], static function() {
    Route::post('/', 'PostController@createPost');
    Route::patch('/{post}', 'PostController@updatePost');
    Route::delete('/{post}','PostController@deletePost');

    Route::group(['prefix' => '{post}/comments' , 'middleware' => 'auth:api'], static function() {
        // @TODO
        Route::post('/', 'CommentController@createPost');
        Route::put('/{post}', 'CommentController@updatePost');
        Route::delete('/{post}','CommentController@deletePost');
    });
});

Route::group(['prefix' => 'posts' , 'middleware' => 'guest'], static function() {
    Route::get('/', 'PostController@listPost');
    Route::get('/{post}', 'PostController@showPost');

    // @TODO
    Route::group(['prefix' => '{post}/comments' , 'middleware' => 'auth:api'], static function() {
        Route::get('/', 'CommentController@listPost');
        Route::get('/{post}', 'CommentController@showPost');
    });
});