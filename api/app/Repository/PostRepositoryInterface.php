<?php
declare(strict_types=1);

namespace App\Repository;

use App\Models\Post;
use Illuminate\Support\Collection;
use App\Repository\Data\Post as PostData;

interface PostRepositoryInterface
{
   public function listAllPost(): Array;

   public function save(array $request): Int;
   public function update(Post $post, array $request): Int;
   public function delete(Post $post): Int;

   public function savePost(PostData $postData): bool;

   public function updatePost(Post $post, PostData $postData): bool;

   public function deletePost(Post $post): Int;
}