<?php
declare(strict_types=1);

namespace App\Repository\Eloquent;

use App\Models\Comment;
use App\Models\Post;
use App\Repository\CommentRepositoryInterface;
use Illuminate\Support\Collection;

class CommentRepository extends BaseRepository implements CommentRepositoryInterface
{
    /**
     * CommentRepository constructor.
     * 
     * @param Comment $model
     */
    public function __construct(Comment $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    /**
     * @return Int
     */
    public function save(Post $post, array $request): Int
    {
        $data = $this->model;
        $data->body = $request['body'];
        $data->commentable_type = Post::class;
        $data->commentable_id = $post->id;
        $data->creator_id = $post->user_id;
        $data->parent_id = null;
        $data->save();

        return $data->id;
    }
    /**
     * @return Int
     */
    public function update(int $id, array $request): Int
    {
        $update = $this->model->find($id);
        $update->body = $request['body'];
        $update->save();

        return $update->id;
    }

    /**
     * @return Int
     */
    public function delete(int $id): Int
    {
        return $this->model->destroy($id);
    }
}