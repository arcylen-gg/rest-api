<?php
declare(strict_types=1);

namespace App\Repository\Data;

class Post
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $image;

    /**
     * @var int
     */
    private $user_id;

    public function __construct(string $title, string $content, int $user_id)
    {
        $this->title = $title;
        $this->content = $content;
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param null|string $image
     *
     * @return Post
     */
    public function setImage(?string $image = null): self
    {
        $this->image = $image;

        return $this;
    }

    public function getSlug(): string
    {
        return str_replace(' ', '-', $this->title);
    }

    /**
     * @return mixed[]
     */
    public function toArray() : array
    {
        return [
            'title' => $this->getTitle(),
            'content' => $this->getContent(),
            'slug' => $this->getSlug(),
            'user_id' => $this->getUserId(),
            'image' => $this->getImage(),
        ];
    }
}
