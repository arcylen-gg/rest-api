<?php
declare(strict_types=1);

namespace App\Repository;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Support\Collection;

interface CommentRepositoryInterface
{
   public function all(): Collection;
   public function save(Post $post, array $request): Int;
   public function update(int $id, array $request): Int;
   public function delete(int $int): Int;
}