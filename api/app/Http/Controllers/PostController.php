<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Repository\PostRepositoryInterface;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Post;
use App\Repository\Data\Post as PostData;

class PostController extends Controller
{
    private $post;

    public function __construct(PostRepositoryInterface $post)
    {
        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listPost()
    {
        return response()->json([
            'data' => $this->post->listAllPost()
        ], Response::HTTP_ACCEPTED);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
        ]);
        if (!$validator->fails()) {
            $return['data'] = $this->post->find($this->post->save($request->all()));
            return response()->json($return, Response::HTTP_ACCEPTED);
        } else {
            return response()->json([
                    'messages' => 'The given data was invalid',
                    'errors' => $validator->messages()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    public function createPost(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'content' => 'required|string',
            'image' => 'nullable|url',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => 'The given data was invalid',
                'errors' => $validator->messages()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $postData = (new PostData(
            $request->get('title'),
            $request->get('content'),
            $user->getAuthIdentifier()
        ))->setImage($request->get('image'));

        $isSaved = $this->post->savePost($postData);

        return response()->json(
            ['data' => $postData],
            $isSaved === true ? Response::HTTP_CREATED : Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }

    public function showPost(Post $post)
    {
        return response()->json(['data' => $post], Response::HTTP_ACCEPTED);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePost(Post $post, Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'content' => 'required|string',
            'image' => 'nullable|url',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => 'The given data was invalid',
                'errors' => $validator->messages()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $postData = (new PostData(
            $request->get('title'),
            $request->get('content'),
            $user->getAuthIdentifier()
        ))->setImage($request->get('image'));

        $isSaved = $this->post->updatePost($post, $postData);

        return response()->json(
            ['data' => $post],
            $isSaved === true ? Response::HTTP_ACCEPTED : Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }

    /**
     * @param Post $post
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function deletePost(Post $post)
    {
        $deletedCount = $this->post->deletePost($post);

        if($deletedCount > 0) {
            return response()->json([], Response::HTTP_ACCEPTED);
        }

        throw new \Exception('Cannot Delete Entity', Response::HTTP_BAD_REQUEST);
    }
}
